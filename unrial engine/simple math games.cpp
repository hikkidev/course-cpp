﻿#include <iostream>
#include <ctime>

using namespace std;

class RevenueMenu
{
public:

	void wannaQuit() {
		system("cls");
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Alright then. Have a great day =P." << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	}

	void inputError() {
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Sorry, but there is no such game so far :(" << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	}

	void mainMenu() {
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Welcome! Choose one of the options." << endl << endl;
		cout << "1.TripleX1." << endl;
		cout << "2.TripleX2." << endl;
		cout << "3.Check the vault." << endl;
		cout << "4.Exit." << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	}

	void tripleX1intro() {
		system("cls");

		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "They said you are the best in this.";
		cout << "I hope they are right. Hurry up! We have little time." << endl << endl;
		cout << "Task: you will get a set of numbers and will have to find their sum and product. Good luck!" << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;

		system("pause");
		system("cls");
	}
	void tripleX2intro() {
		system("cls");

		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "OK. Listen up. This one should be hard, but I believe in you. Meet me at our place whenever you are done." << endl << endl;
		cout << "Task: break through the security system." << endl;
		cout << "* You will have to enter a code containting three numbers." << endl;
		cout << "* Each time you will get the sum and the product of those numbers." << endl;
		cout << "* Good luck!" << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;

		system("pause");
		system("cls");
	}

	void tripleX1() {
		int firstOperand = 0;
		int secondOperand = 0;
		int thirdOperand = 0;
		int operandsSumInput = 0, operandsProductInput = 0;

		firstOperand = rand() % 10;
		secondOperand = rand() % 10;
		thirdOperand = rand() % 10;

		int operandsSum = firstOperand + secondOperand + thirdOperand;
		int operandsProduct = firstOperand * secondOperand * thirdOperand;

		do {
			cout << "----------------------------------------------------------" << endl;
			cout << firstOperand << "               ";
			cout << secondOperand << "               ";
			cout << thirdOperand << "               " << endl;
			cout << "----------------------------------------------------------" << endl;

			cout << "Enter the right answers" << endl;

			cout << "sum: ";
			cin >> operandsSumInput;

			cout << "product: ";
			cin >> operandsProductInput;

			if (operandsSumInput != operandsSum && operandsProductInput != operandsProduct) {
				cout << "Access denied" << endl << endl;
			}
		} while (operandsSumInput != operandsSum && operandsProductInput != operandsProduct);

		cout << "Access gained" << endl;

		system("pause");
		system("cls");
	}
	void tripleX2() {
		int firstOperand = 0;
		int secondOperand = 0;
		int thirdOperand = 0;
		firstOperand = rand() % 5;
		secondOperand = rand() % 5;
		thirdOperand = rand() % 5;
		int operandsSum = firstOperand + secondOperand + thirdOperand;
		int operandsProduct = firstOperand * secondOperand * thirdOperand;
		int finish = false;

		do {
			cout << "---------------------------------------------------------------" << endl;
			cout << "+ There are three numbers in the code" << endl;
			cout << "+ The numbers should be entered through 'space' button." << endl;
			cout << "+ The sum of the numbers is: " << operandsSum << endl;
			cout << "+ The product of the numbers is: " << operandsProduct << endl;
			cout << "---------------------------------------------------------------" << endl;

			int firstOperandInput = 0;
			int	secondOperandInput = 0;
			int	thirdOperandInput = 0;

			cout << "+ Enter the Code: ";
			cin >> firstOperandInput;
			cin >> secondOperandInput;
			cin >> thirdOperandInput;

			int operandsInputSum = firstOperandInput + secondOperandInput + thirdOperandInput;
			int operandsInputProduct = firstOperandInput * thirdOperandInput * secondOperandInput;
			if (operandsInputSum == operandsSum && operandsInputProduct == operandsProduct) {
				finish = false;
			}
			if (operandsInputSum != operandsSum && operandsInputProduct != operandsProduct) {
				finish = true;
				cout << "Access denied" << endl;
			}
		} while (finish);
		cout << "Access gained" << endl;
		system("pause");
		system("cls");
	}

	void tripleX1finish() {
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Congratulations! You have opened the safe." << endl;
		cout << "The amount of money gotten: " << revenue << "$" << endl;
		revenueSum += revenue;
		cout << "Moeny in the vault: " << revenueSum << "$" << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		system("pause");
		system("cls");
	}
	void tripleX2finish() {
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Well done! You have broken through." << endl;
		cout << "The amount of money gotten: " << revenue << "$" << endl;
		revenueSum += revenue;
		cout << "Moeny in the vault: " << revenueSum << "$" << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		system("pause");
		system("cls");
	}

	int getUserChoise() {
		int key = 0;
		cin >> key;
		return key;
	}

	void showRevenue() {
		if (revenueSum == 0) {
			cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
			cout << "It's seem like there is nothing there." << endl;
			cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl << endl;
		}
		else if (revenueSum > 0)
		{
			cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
			cout << "You currently have: " << revenueSum << "$" << endl;
			cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		}
	}

private:
	int revenue = rand() % 10000;
	int revenueSum = 0;
};


int main()
{
	srand(time(0));
	int maxlvl = 10;
	RevenueMenu revMenu;

	revMenu.mainMenu();
	while (int key = revMenu.getUserChoise()) {
		switch (key)
		{
		case 1:
		{
			revMenu.tripleX1intro();

			for (int i = 1; i <= maxlvl; i++) {
				cout << "Level - " << i << endl;
				revMenu.tripleX1();
			}

			revMenu.tripleX1finish();
			revMenu.mainMenu();

			break;
		}
		case 2:
		{
			revMenu.tripleX2intro();

			for (int i = 1; i <= maxlvl; i++) {
				cout << "Level - " << i << endl;
				revMenu.tripleX2();
			}

			revMenu.tripleX2finish();
			revMenu.mainMenu();
			break;
		}
		case 3:
		{
			revMenu.showRevenue();
			break;
		}
		case 4:
		{
			revMenu.wannaQuit();
			return 0;
		}
		default:
		{
			revMenu.inputError();
			break;
		}

		}
	}

	return 0;
}



