﻿#include <iostream>
#include <ctime>
#include <string>
#include <conio.h>


using namespace std;

	class RevenueMenu
{
public:



	void wannaQuit() {
		system("cls");
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Alright then. Have a great day =P." << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	}

	void inputError() {
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Sorry, but there is no such game so far :(" << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	}

	void mainMenu() {
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Welcome! Choose one of the options." << endl << endl;
		cout << "1.TripleX1." << endl;
		cout << "2.TripleX2." << endl;
		cout << "3.Hangman" << endl;
		cout << "4.Snake" << endl;
		cout << "5.Check the vault." << endl;
		cout << "6.Exit." << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	}
	
	

	void tripleX1intro() {
		system("cls");

		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "They said you are the best in this.";
		cout << "I hope they are right. Hurry up! We have little time." << endl << endl;
		cout << "Task: you will get a set of numbers and will have to find their sum and product. Good luck!" << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;

		system("pause");
		system("cls");
	}
	void tripleX2intro() {
		system("cls");

		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "OK. Listen up. This one should be hard, but I believe in you. Meet me at our place whenever you are done." << endl << endl;
		cout << "Task: break through the security system." << endl;
		cout << "* You will have to enter a code containting three numbers." << endl;
		cout << "* Each time you will get the sum and the product of those numbers." << endl;
		cout << "* Good luck!" << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;

		system("pause");
		system("cls");
	}
	void hangmanIntro() {
		system("cls");

		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Hello, buddy." << endl;
		cout << "This is just a simple hangman, which means the rules are simple." << endl;
		cout << "You will get a word and will have to guess it! That's all." << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		system("pause");
		system("cls");
}
	void snakeIntro()
	{
		system("cls");

		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "+ Hello" << endl;
		cout << "+ Thissss issss a ssssnake game" << endl;
		cout << "+ The rulessss you know already." << endl;
		cout << "+ Controls: 'w' - up, 's' - down, 'd' - right, 'a' - left , 'x' - for quiting the game" << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		system("pause");
		system("cls");
	}

	void tripleX1() {
		int firstOperand = 0;
		int secondOperand = 0;
		int thirdOperand = 0;
		int operandsSumInput = 0, operandsProductInput = 0;

		firstOperand = rand() % 10 + 1;
		secondOperand = rand() % 10 + 1;
		thirdOperand = rand() % 10 + 1;

		int operandsSum = firstOperand + secondOperand + thirdOperand;
		int operandsProduct = firstOperand * secondOperand * thirdOperand;

		do {
			cout << "----------------------------------------------------------" << endl;
			cout << firstOperand << "               ";
			cout << secondOperand << "               ";
			cout << thirdOperand << "               " << endl;
			cout << "----------------------------------------------------------" << endl;

			cout << "Enter the right answers" << endl;

			cout << "sum: ";
			cin >> operandsSumInput;

			cout << "product: ";
			cin >> operandsProductInput;

			if (operandsSumInput != operandsSum && operandsProductInput != operandsProduct) {
				cout << "Access denied" << endl << endl;
			}
		} while (operandsSumInput != operandsSum && operandsProductInput != operandsProduct);

		cout << "Access gained" << endl;

		system("pause");
		system("cls");
	}
	void tripleX2() {
		int firstOperand = 0;
		int secondOperand = 0;
		int thirdOperand = 0;
		firstOperand = rand() % 5 + 1;
		secondOperand = rand() % 5 + 1;
		thirdOperand = rand() % 5 + 1;
		int operandsSum = firstOperand + secondOperand + thirdOperand;
		int operandsProduct = firstOperand * secondOperand * thirdOperand;
		int finish = false;

		do {
			cout << "---------------------------------------------------------------" << endl;
			cout << "+ There are three numbers in the code" << endl;
			cout << "+ The numbers should be entered through 'space' button." << endl;
			cout << "+ The sum of the numbers is: " << operandsSum << endl;
			cout << "+ The product of the numbers is: " << operandsProduct << endl;
			cout << "---------------------------------------------------------------" << endl;

			int firstOperandInput = 0;
			int	secondOperandInput = 0;
			int	thirdOperandInput = 0;

			cout << "+ Enter the Code: ";
			cin >> firstOperandInput;
			cin >> secondOperandInput;
			cin >> thirdOperandInput;

			int operandsInputSum = firstOperandInput + secondOperandInput + thirdOperandInput;
			int operandsInputProduct = firstOperandInput * thirdOperandInput * secondOperandInput;
			if (operandsInputSum == operandsSum && operandsInputProduct == operandsProduct) {
				finish = false;
			}
			if (operandsInputSum != operandsSum && operandsInputProduct != operandsProduct) {
				finish = true;
				cout << "Access denied" << endl;
			}
		} while (finish);
		cout << "Access gained" << endl;
		system("pause");
		system("cls");
	}
	void hangman() {

			//int livesLeft = 0;
			char input;
			const int size = 5;
			string wordList[size] = { "hello", "my", "name", "is", "agaugu" };
			string word = wordList[rand() % size];
			size_t currentAttempt, maxAttemtp = word.length() + 2;
			string unknown(word.length(), '*');
			cout << "---------------------------------------------" << endl;	
			cout << "+ Lives avalible: " << maxAttemtp << endl;
			cout << "+ Your word is - " << unknown << endl;
			cout << "+ Enter the first letter" << endl;

			for (currentAttempt = 0; currentAttempt < maxAttemtp; currentAttempt++)
			{
				//livesLeft = maxAttemtp;
				cin >> input;
				int pos = word.find(input);
				if (pos != string::npos) {
					unknown[pos] = input;
					word[pos] = NULL;
					cout << unknown << endl;
				}
			/*	if (pos == string::npos)
				{
					livesLeft - 1;
					cout << "+ Lives left - " << livesLeft<< endl;
				} */
			}
			if (currentAttempt = maxAttemtp)
			{
				system("pause");
				system("cls");
				cout << "~~~~~~~~~~~~~~~~~~~~~~~" << endl;
				cout << "+ Game over." << endl;
				cout << "+ You are out of lives." << endl;
				cout << "+ The wor was '" << word << "'" << endl;
				cout << "~~~~~~~~~~~~~~~~~~~~~~~" << endl;
				system("pause");
				system("cls");
			}
		}
	


	void tripleX1finish() {
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Congratulations! You have opened the safe." << endl;
		cout << "The amount of money gotten: " << revenue << "$" << endl;
		revenueSum += revenue;
		cout << "Moeny in the vault: " << revenueSum << "$" << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		system("pause");
		system("cls");
	}
	void tripleX2finish() {
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Well done! You have broken through." << endl;
		cout << "The amount of money gotten: " << revenue << "$" << endl;
		revenueSum += revenue;
		cout << "Moeny in the vault: " << revenueSum << "$" << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		system("pause");
		system("cls");
	}
	void hangmanFinish() {
		system("cls");
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Congratulations on guessing this word!" << endl;
		cout << "The amount of money gotten: " << revenue << "$" << endl;
		revenueSum += revenue;
		cout << "Money in the vault: " << revenueSum << "$" << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		system("pause");
		system("cls");
	}
	
	
	int getUserChoise() {
		int key = 0;
		cin >> key;
		return key;
	}

	void showRevenue() {
		if (revenueSum == 0) {
			cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
			cout << "It's seem like there is nothing there." << endl;
			cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl << endl;
		}
		else if (revenueSum > 0)
		{
			cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
			cout << "You currently have: " << revenueSum << "$" << endl;
			cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		}
	}

	
private:
	int revenue = rand() % 10000;

	int revenueSum = 0;

	
};
	
	int score = 0;
	int bestScore = 0;	
	int x, y, fruitX, fruitY;
	int tailX[100], tailY[100];
	int ntail;
	int width = 35;
	int height = 20;

	bool gameOver;
	
	enum controls { STOP = 0, LEFT, RIGHT, UP, DOWN };
	controls cont;
		
	void snakeGameSetup()
	{
		gameOver = false;
		cont = STOP;
		x = width / 2 - 1;
		y = height / 2 ;
		fruitX = rand() % width - 2;
		if (fruitX <= width || fruitX >= width)
		{
			fruitX = rand() % width;
		}
		fruitY = rand() % height;
		
	}
	void snakeMapDrawing() 
	{
			system("cls");
			cout << endl;
			for (int j = 0; j < 2; j++)
				cout << " ";
			for (int i = 0; i < width + 3; i++)
				cout << "#";
			cout << endl;


			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					if (j == 0 || j == width - 1)
					{
						cout << "  ";
						cout << "#";
					}
					if (i == y && j == x)
						cout << "0";
					else if (i == fruitY && j == fruitX)
						cout << "F";
					else {
						bool print = false;
						for (int k = 0; k < ntail; k++)
						{
							if (tailX[k] == j && tailY[k] == i)
							{
								print = true;
								cout << "o";
							}
						}
						if (!print)
							cout << " ";
					}
				}
				cout << endl;
			}
			for (int j = 0; j < 2; j++)
				cout << " ";
			for (int i = 0; i < width + 3; i++)
				cout << "#";
			cout << endl;
			cout << "  SCORE: " << score << endl;
	
	}
	void snakeUserInput()
	{
	if (_kbhit())
	{
		switch (_getch())
		{
		case 'w':
		{
			cont = UP;
			break;
		}
		case 's':
		{
			cont = DOWN;
			break;
		}
		case 'd':
		{
			cont = RIGHT;
			break;
		}
		case 'a':
		{
			cont = LEFT;
			break;
		}
		case 'x':
			gameOver = true;
			break;
		}
	
	}

	}
	void snakeLogic()
	{
		int tailPrevX = tailX[0];
		int tailPrevY = tailY[0];
		int tailPrev2X, tailPrev2Y;
		tailX[0] = x;
		tailY[0] = y;
		for(int i = 1; i < ntail; i++)
		{
			tailPrev2X = tailX[i];
			tailPrev2Y = tailY[i];
			tailX[i] = tailPrevX;
			tailY[i] = tailPrevY;
			tailPrevX = tailPrev2X;
			tailPrevY = tailPrev2Y;
		}

		switch (cont)
		{
		
		case LEFT:
		{
			x--;

			break;
		}
		case RIGHT:
		{
			x++;
			break;
		}
		case UP:
		{
			y--;
			break;
		}
		case DOWN:
		{
			y++;
			break;
		}
		}
		
		if (x >= width - 1)
			x = 0;
		else if (x < 0)
			x = width - 2;
	 	if (y >= height - 1)
			y = 0;
		else if (y < 0)
			y = height - 1;
		for (int i = 0; i < ntail; i++)
		{
			if(tailX[i] == x && tailY[i] == y)
			{
				gameOver = true;
			}
		}
		if (x == fruitX && y == fruitY) 
		{
			score += 100;
			fruitX = rand() % width - 2;
			if (fruitX <= width || fruitX >= width)
			{
				fruitX = rand() % width;
			}
			fruitY = rand() % height;
			ntail++;
		}
	}

	void snake()
	{
			while (!gameOver)
			{
				snakeMapDrawing();
				snakeUserInput();
				snakeLogic();
			} 
	}

	void snakeFInish()
	{
		system("cls");
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Congratulationssssss!" << endl;
		cout << "Your sssssscore isss: " << score  << " points" << endl;
		if (bestScore <= score)
		{
			bestScore = score;
		}
		cout << "Your besssst ssscore issss: " << bestScore  <<" points" << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		system("pause");
		system("cls");
	}


	
int main()
{
	int maxlvl = 10;
	RevenueMenu revMenu;
	
	srand(time(0));
	
	revMenu.mainMenu();
	while (int key = revMenu.getUserChoise()) {
		switch (key)
		{
		case 1:
		{
			revMenu.tripleX1intro();

			for (int i = 1; i <= maxlvl; i++) {
				cout << "Level - " << i << endl;
				revMenu.tripleX1();
			}

			revMenu.tripleX1finish();
			revMenu.mainMenu();

			break;
		}
		case 2:
		{
			revMenu.tripleX2intro();

			for (int i = 1; i <= maxlvl; i++) {
				cout << "Level - " << i << endl;
				revMenu.tripleX2();
			}

			revMenu.tripleX2finish();
			revMenu.mainMenu();
			break;
		}
		case 3:
		{
			revMenu.hangmanIntro();
			revMenu.hangman();
			revMenu.hangmanFinish();
			revMenu.mainMenu();
			break;
		}
		case 4: 
		{
			revMenu.snakeIntro();
			snakeGameSetup();
			snake();
			snakeFInish();
			revMenu.mainMenu();
			break;
			
		}
		case 5:
		{
			revMenu.showRevenue();
			break;
		}
		case 6:
		{
			revMenu.wannaQuit();
			return 0;
		}
		default:
		{
			revMenu.inputError();
			break;
		}
		}
	}
	return 0;
}
